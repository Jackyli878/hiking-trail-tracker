from django.urls import path
from projects.views import project_list, project_details, create_project
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path("", login_required(project_list), name="list_projects"),
    path("<int:id>/", login_required(project_details), name="show_project"),
    path(
        "create/",
        login_required(create_project),
        name="create_project",
    ),
]
